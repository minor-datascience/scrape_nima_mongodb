# Goodreads scrape and image rater
These repository is a method of retrieving data from the Goodreads API, retrieving more data than the standard API function.  
After that you can rate images with NIMA, which puts the ratings back in the MongoDB.

## Installation
Install all needed Python packages by running `pip install -r requirements.txt`.  
Also, install MongoDB.

## Usage
First run `tools/KaggleCSVToDatabase.py` to insert a new database in MongoDB.  
Then run `Webscraper.py` for as long as you want to scrape book database.  
After that run `evaluate_mobilenet.py` to rate all images with NIMA and put all data in MongoDB.  

## API Keys
Goodreads API keys can be retrieved from [here](https://www.goodreads.com/api/keys)  
Jorts keys: 
```   
key: fVfK4pnsbfKiS92eIqcGg  
secret: h3c7ezID7pCu4n2thKTBqwnbBftmFFYJST62NqBNQY
```   

## Documentation
[Goodreads API](https://www.goodreads.com/api)  
[Goodreads sample book page](https://www.goodreads.com/book/show/23692271)  
[Beautifulsoup quick start guide](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#quick-start)  
[LXML Documentation](https://lxml.de/api/lxml.etree._Element-class.html)  

## Links
Download MongoDB: [link](https://www.mongodb.com/try/download/community)  
MongoDB tutorial to the point: [link](https://www.w3schools.com/python/python_mongodb_getstarted.asp)  
Convert SQL to Mongo: [link](http://www.querymongo.com/)  
Standard MongoDB connection URL: [mongodb://localhost:27017/](mongodb://localhost:27017/)

## Example code
Working Java code, replace keys: [link](https://github.com/davecahill/goodreads-oauth-sample)  







