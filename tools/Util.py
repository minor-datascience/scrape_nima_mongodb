# script file with utility functions, used mainly in the webscraper
import re


def get_regex_group(text, regex, group):
    pattern = re.compile(regex)
    match = pattern.search(text)
    return match.group(group)


def after(string, text):
    result = ""
    try:
        return string.split(text, 1)[1]
    except Exception:
        return "nope"


def before(string, text):
    result = ""
    try:
        return string.split(text, 1)[0]
    except Exception:
        return "nope"


def remove_image_scaling(image_string):
    if "._SY" not in image_string and "._SX" not in image_string:
        return image_string

    pattern = re.compile(".*(\..*)\.jpg")
    match = pattern.match(image_string)
    if not match:
        return image_string
    to_remove = match.group(1)  # string captured in group 1: something like ._SY475_
    return re.sub(to_remove, "", image_string)


def get_string(navigable_string):
    return str(navigable_string).strip()


def print_object(obj):
    for attr in dir(obj):
        print("obj.%s = %r" % (attr, getattr(obj, attr)))


def print_dict(dict_to_print):
    for key in dict_to_print.keys():
        print(key + ": " + dict_to_print[key])


# test util functions
"""
text = "https://www.goodreads.com/author/show/1905282.Juju_Sundin"
regex = r"\/author\/show\/(.*)\.(.*)"
matches = re.search(regex, text)
print("Matches:", matches)
print("Group 1:", matches.group(1))
res1 = matches.group(2)
print("res", res1)
res2 = res1.replace("_", " ")
print("res2:", res2)

result = get_regex_group(text, regex, 1)
print("Result:", result)
"""
