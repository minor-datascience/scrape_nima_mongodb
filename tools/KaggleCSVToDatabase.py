# import the kaggle database csv into mongodb
# (needed for the webscraper to iterate through the isbns)
import pymongo
import pandas as pd

DATABASE_NAME = "goodreads"
COLLECTION_NAME = "kaggle_data"

db_client = pymongo.MongoClient("mongodb://localhost:27017/")

db = db_client[DATABASE_NAME]
db_collection = db[COLLECTION_NAME]

if COLLECTION_NAME not in db.list_collection_names():
    print("Importing CSV in database.")
    # create data frame from csv
    df = pd.read_csv("../data_cleaned.csv")
    # convert df to dict list as 'records', which can be inserted in mongodb
    db_collection.insert_many(df.to_dict("records"))
else:
    print("Collection already exists, skipping CSV import.")

