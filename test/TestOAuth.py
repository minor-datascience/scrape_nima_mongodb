# testing OAuth, which the goodreads API uses for some of its functions
from rauth import OAuth1Service, OAuth1Session

# create oauth instance with developer key and secret
########################################################################################################################
CONSUMER_KEY = "fVfK4pnsbfKiS92eIqcGg"
CONSUMER_SECRET = "h3c7ezID7pCu4n2thKTBqwnbBftmFFYJST62NqBNQY"

ACCESS_TOKEN = "4mpP6UzzfDBwUPI9uOyVw"
ACCESS_TOKEN_SECRET = "fD6rCh6qJSBRhljJSHkcNt6madZITQk66bz8XfSr2gQ"

new_session = OAuth1Session(
    consumer_key=CONSUMER_KEY,
    consumer_secret=CONSUMER_SECRET,
    access_token=ACCESS_TOKEN,
    access_token_secret=ACCESS_TOKEN_SECRET,
)

data = {'name': 'to-read', 'book_id': 48570454}
response = new_session.post('https://www.goodreads.com/shelf/add_to_shelf.xml', data)

# response = new_session.post("https://www.goodreads.com/book/isbn/006288168X?format=xml&key=fVfK4pnsbfKiS92eIqcGg")
if response.status_code != 201:
    print("Error:", response.status_code)
else:
    print("Reponse:", response)
