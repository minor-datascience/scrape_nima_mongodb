# testing OAuth, which the goodreads API uses for some of its functions
from rauth import OAuth1Service

# create oauth instance with developer key and secret
########################################################################################################################
CONSUMER_KEY = "fVfK4pnsbfKiS92eIqcGg"
CONSUMER_SECRET = "h3c7ezID7pCu4n2thKTBqwnbBftmFFYJST62NqBNQY"

goodreads = OAuth1Service(
    consumer_key=CONSUMER_KEY,
    consumer_secret=CONSUMER_SECRET,
    name='goodreads',
    request_token_url='https://www.goodreads.com/oauth/request_token',
    authorize_url='https://www.goodreads.com/oauth/authorize',
    access_token_url='https://www.goodreads.com/oauth/access_token',
    base_url='https://www.goodreads.com/'
)

# ask goodreads for request token
########################################################################################################################
request_token, request_token_secret = goodreads.get_request_token(header_auth=True)
print("Request token:", request_token)
print("Request token secret:", request_token_secret)

# exchange request token for access token
########################################################################################################################
authorize_url = goodreads.get_authorize_url(request_token)
print("Visit this URL: ", authorize_url)
accepted = input("Type something when authorized.")  # blocking function waits for input
session = goodreads.get_auth_session(request_token, request_token_secret)
ACCESS_TOKEN = session.access_token
ACCESS_TOKEN_SECRET = session.access_token_secret
print("Access token:", ACCESS_TOKEN)
print("Access token secret:", ACCESS_TOKEN_SECRET)

# use the session which has the access token to use the API
########################################################################################################################
# book_id 631932 is "The Greedy Python"
data = {'name': 'to-read', 'book_id': 631932}
response = session.post('https://www.goodreads.com/shelf/add_to_shelf.xml', data)


