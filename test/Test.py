import datetime

start_time = datetime.datetime.now()

x = 0
for i in range(1000000):
   x += i

end_time = datetime.datetime.now()

time_diff = (end_time - start_time)
execution_time = time_diff.total_seconds() * 1000

print(execution_time)