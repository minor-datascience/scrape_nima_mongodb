# webscraper for goodreads, requires mongodb with a database with all the isbn to iterate
import datetime
import traceback

import pymongo
import urllib3
from lxml import etree
from bs4 import BeautifulSoup
from tools.Util import *

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def is_isbn_in_db(isbn_to_check):
    return db_collection_mine.count_documents({"isbn": isbn_to_check}) > 0


def parse_book_details(isbn):
    start_time = datetime.datetime.now()
    # setup scraping tools########################################################################################
    try:
        url = "https://www.goodreads.com/book/isbn_to_id?isbn=" + isbn
        http = urllib3.PoolManager()
        response = http.request('GET', url)
        response_text = response.data.decode()

        soup = BeautifulSoup(response_text, 'html.parser')
        tree = etree.HTML(response_text)
    except Exception:
        print("Failed to setup scraping tools for ISBN", isbn)
        return

    # initialize result########################################################################################
    book = {}
    book["isbn"] = isbn
    book["scraped_link"] = url

    # title########################################################################################
    try:
        tag = soup.find("h1")
        book_title = get_string(tag.contents[0])
        book["title"] = book_title
    except:
        book["title"] = ERROR_VALUE

    # cover link########################################################################################
    try:
        tag = soup.select("img#coverImage")[0]  # use css selector to get by id
        book_cover_image = tag["src"]
        book_cover_image = remove_image_scaling(book_cover_image)
        book["cover_link"] = book_cover_image
    except:
        book["cover_link"] = ERROR_VALUE

    # average rating########################################################################################
    try:
        tag = soup.find(attrs={"itemprop": "ratingValue"})
        book_rating = get_string(tag.contents[0])
        book["average_rating"] = book_rating
    except:
        book["average_rating"] = ERROR_VALUE

    # amount of ratings per star########################################################################################
    try:
        xpath = "//script[contains(@type,'protovis')]"
        tag = tree.xpath(xpath)[0]
        tag_text = get_string(tag.text)
        pattern = re.compile("renderRatingGraph\(\[(.*)\]\)")
        match = pattern.match(tag_text)
        result = match.group(1)  # string captured in group 1: something like ._SY475_
        result = result.split(", ")
        book["5_star_count"] = result[0]
        book["4_star_count"] = result[1]
        book["3_star_count"] = result[2]
        book["2_star_count"] = result[3]
        book["1_star_count"] = result[4]
    except:
        book["5_star_count"] = ERROR_VALUE
        book["4_star_count"] = ERROR_VALUE
        book["3_star_count"] = ERROR_VALUE
        book["2_star_count"] = ERROR_VALUE
        book["1_star_count"] = ERROR_VALUE

    # writer########################################################################################
    try:
        xpath = "//meta[contains(@property,'books:author')]"
        tag = tree.xpath(xpath)[0]
        tag_text = tag.get("content")
        book["writer"] = get_regex_group(tag_text, "\/author\/show\/(.*)\.(.*)", 2).replace("_", " ")
        book["writer_id"] = get_regex_group(tag_text, "\/author\/show\/(.*)\.(.*)", 1)
    except:
        book["writer"] = ERROR_VALUE
        book["writer_id"] = ERROR_VALUE

    # genres########################################################################################
    try:
        book_genre_list_text = get_regex_group(response_text, 'googletag\.pubads\(\)\.setTargeting\("shelf", \[(.*)\]',
                                               1)
        book_genre_list_text = book_genre_list_text.replace('"', "")
        book_genre_list = book_genre_list_text.split(",")
        for x in range(0, 5):  # max 15 genres, but some have less, so 5
            key = "genre_" + str(x)
            book[key] = book_genre_list[x]
    except:
        for x in range(0, 5):  # max 15 genres
            key = "genre_" + str(x)
            book[key] = ERROR_VALUE

    # amount of written reviews########################################################################################
    try:
        tag = soup.find(attrs={"itemprop": "reviewCount"})
        book_written_review_count = tag.get("content")
        book["review_count"] = book_written_review_count
    except:
        book["review_count"] = ERROR_VALUE

    # book format: paperback, etc########################################################################################
    try:
        tag = soup.find(attrs={"itemprop": "bookFormat"})
        book_format = get_string(tag.contents[0])
        book["format"] = book_format
    except:
        book["format"] = ERROR_VALUE

    # amount of pages########################################################################################
    try:
        tag = soup.find(attrs={"itemprop": "numberOfPages"})
        book_format = before(get_string(tag.contents[0]), " pages")
        book["amount_of_pages"] = book_format
    except:
        book["amount_of_pages"] = ERROR_VALUE

    # published date########################################################################################
    try:
        book["published"] = get_regex_group(response_text, "Published\s*(\d\d\d\d)", 1)
    except:
        book["published"] = ERROR_VALUE

    # first published date  ########################################################################################
    try:
        book["first_published"] = get_regex_group(response_text, "first published.* (\d\d\d\d)", 1)
    except:
        book["first_published"] = ERROR_VALUE

    # finishing up########################################################################################
    end_time = datetime.datetime.now()

    time_diff = (end_time - start_time)
    execution_time = int(time_diff.total_seconds() * 1000)
    book["parse_duration"] = execution_time

    return book


# setup database connection
DATABASE_NAME = "goodreads"
COLLECTION_KAGGLE_NAME = "kaggle_data"
COLLECTION_MINE_NAME = "the_data_test"
ERROR_VALUE = "-1"

db_client = pymongo.MongoClient("mongodb://localhost:27017/")

db = db_client[DATABASE_NAME]
db_collection_kaggle = db[COLLECTION_KAGGLE_NAME]
db_collection_mine = db[COLLECTION_MINE_NAME]

# select all isbns
isbn_cursor = db_collection_kaggle.find({}, {
    "isbn": 1
})

# fill parsed book result list
book_list = []  # result list
count = 0  # count for stopping condition
for isbn_dict in isbn_cursor:  # maybe iterate ordered for existing values

    isbn = str(isbn_dict["isbn"])
    if is_isbn_in_db(isbn):
        # print("ISBN", isbn, "already exists in database")
        continue

    try:
        parsed_book = parse_book_details(isbn)

        # add dict to database
        db_collection_mine.insert_one(document=parsed_book)

        # add dict with parsed values to result list
        book_list.append(parsed_book)

        # print("Parsed book details:")
        # print_dict(parsed_book)
        print("Parsed in", parsed_book["parse_duration"], "ms:", parsed_book)

    except Exception as e:
        print("Parsing FAILED for book with ISBN:", isbn, "with error:", e)
        # removed: exceptions should now be thrown on key level

        # # add error dict to database
        # error_book = {"isbn": isbn, "parsing_success": False, "parsing_error": str(e)}
        # db_collection_mine.insert_one(document=error_book)
        #
        # # add error dict to result list
        # book_list.append(error_book)
        # print(traceback.format_exc())

    # stop condition
    count += 1
    if count > 50:
        break

# evaluate parsed results
# for book_dict in book_list:
#     print("Inserting into database:")
#     for key in book_dict.keys():
#         print(key, ": ", book_dict[key])

# insert all parsed results into database
# db_collection_mine.insert_many(book_list)

# test parse
# book = parse_book_details("9781509858637")